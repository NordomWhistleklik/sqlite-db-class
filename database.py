#!/usr/bin/env python

import sqlite3


class DBConnection(object):
    """
    Opens a SQLite database connection.
    
    Best used with a `with` statement, where a cursor is returned and
    committing and closing the connection are handled automatically:

    >>> with DBConnection(location) as c:
            c.execute(sql_statement)    
    """
    
    def __init__(self,location):
        self.conn = sqlite3.connect(location)
        self.cur = self.conn.cursor()
        
    def __enter__(self):
        return self.cur
    
    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()

    
        
